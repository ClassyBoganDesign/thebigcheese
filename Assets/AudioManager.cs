﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioClips[] audioClips;

    bool fadeIn = false;
    bool fadeOut = false;

	// Use this for initialization
	void Start () {
		foreach (var a in audioClips)
        {
            //gets values from array, creates sources and gives the new sources those values
            if (!a.audioSource)
                a.audioSource = ((a.reference) ? a.reference : gameObject).AddComponent<AudioSource>();
            int debugClip = Random.Range(0, a.audioClip.Length - 1);
            Debug.Log(debugClip);
            a.audioSource.clip = a.audioClip[Random.Range(0,a.audioClip.Length-1)];
            a.audioSource.volume = a.audioVolume;
            a.audioSource.loop = a.isLooping;
            if (a.randomLoop) { a.isLooping = false; }
            if (a.playOnAwake) { a.audioSource.Play();}
        }
	}
	
    public void PlayAudio (string name)
    {
        AudioClips clipToUse = findClipByName(name);
        if (clipToUse != null)
        {
            AudioClip c = clipToUse.audioClip[Random.Range(0, clipToUse.audioClip.Length-1)];
            clipToUse.audioSource.clip = c;
            clipToUse.audioSource.Play();
            if (clipToUse.randomLoop)
            {
                StartCoroutine(DelayedPlay(name, c.length));
            }
        }
       
    }

    IEnumerator DelayedPlay(string name, float delay)
    {
        yield return new WaitForSeconds(delay);
        PlayAudio(name);
    }



    public void PauseAudio (string name)
    {
        AudioClips clipToUse = findClipByName(name);
        if (clipToUse != null) { clipToUse.audioSource.Pause(); }
    }

    public void unPauseAudio(string name)
    {
        AudioClips clipToUse = findClipByName(name);
        if (clipToUse!=null) { clipToUse.audioSource.UnPause(); }
    }

    public IEnumerator FadeAudio(string name, float duration, float targetVolume)
    {
        
        AudioClips clipToUse = findClipByName(name);
        
        float startVol = clipToUse.audioVolume;

        Debug.Log(clipToUse.audioName);
        Debug.Log(duration);
        Debug.Log(targetVolume);
        Debug.Log(startVol);
        if (targetVolume > clipToUse.audioVolume)
                {
                    fadeIn = true;
                    Debug.Log("fade in");
                }
                else if (targetVolume < clipToUse.audioVolume)
                {
                    fadeOut = true;
                    Debug.Log("fade out");
                }
                if (fadeIn)
                {
                    while (clipToUse.audioVolume < targetVolume)
                    {
                        clipToUse.audioSource.volume += (targetVolume - startVol) * Time.deltaTime / duration;
                        yield return null;
                    }
                    if (clipToUse.audioVolume <= targetVolume) { fadeIn = false; }
                }
                if (fadeOut)
                {
                    while (clipToUse.audioVolume > targetVolume)
                    {
                        clipToUse.audioSource.volume -= (startVol - targetVolume) * Time.deltaTime / duration;
                        yield return null;
                    }
                    if (clipToUse.audioVolume <= targetVolume) { fadeOut = false; }
                }
            
    }

    AudioClips findClipByName(string clipname)
    {
        string n = clipname.ToLower();
        foreach (var a in audioClips)
        {
            string audioName = a.audioName.ToLower();
            if (audioName == n) { return a; }

        }
        return null;
    }
    public void StopAudio (string name)
    {
        AudioClips clipToUse = findClipByName(name);
        if (clipToUse != null) { clipToUse.audioSource.Stop(); }
    }

    public void AudioVolume (string name, float volume)
    {
        AudioClips clipToUse = findClipByName(name);
        volume = Mathf.Clamp(volume, 0f, 1f);
        clipToUse.audioSource.volume = volume;
    }
   
    public void LoopAudio (string name)
    {
        AudioClips clipToUse = findClipByName(name);
        clipToUse.audioSource.loop = true;
    }

    public void UnloopAudio (string name)
    {
        AudioClips clipToUse = findClipByName(name);
        clipToUse.audioSource.loop = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCosmeticController : MonoBehaviour {

    public Transform head;
    public string defaultHatID;
    public Cosmetic currentHatCosmetic;
    public string defaultStacheID;
    public Cosmetic currentStacheCosmetic;

    public Transform spatula;
    public string defaultSpatulaID;
    public Cosmetic currentSpatulaCosmetic;

    public Transform knife;
    public string defaultKnifeID;
    public Cosmetic currentKnifeCosmetic;

    CosmeticObjPool cop;
    Head h;

	// Use this for initialization
	void Start () {
        cop = FindObjectOfType<CosmeticObjPool>();
        h = FindObjectOfType<Head>();
        Invoke("EquipDefaults", 0.5f);
	}

    void EquipDefaults()
    {
        GiveMeHat(defaultHatID);
        GiveMeStache(defaultStacheID);
        GiveMeSpatula(defaultSpatulaID);
        GiveMeKnife(defaultKnifeID);
    }
    public void GiveMeHat(string HatID)
    {
        if (currentHatCosmetic)
        {
            cop.Recall(currentHatCosmetic);
            currentHatCosmetic = null;
        }
        GameObject tempHatObj = cop.GiveMe(HatID);
        if (tempHatObj)
        {
            Cosmetic tempHat = tempHatObj.GetComponent<Cosmetic>();
            if (tempHat)
            {

                currentHatCosmetic = tempHat;
                currentHatCosmetic.transform.parent = head;
                currentHatCosmetic.transform.localPosition = Vector3.zero;
                currentHatCosmetic.transform.localEulerAngles = Vector3.zero;
            }
        }
        else
        {

        }

        
    }

    public void GiveMeStache(string CosID)
    {
        if (currentStacheCosmetic)
        {
            cop.Recall(currentStacheCosmetic);
            currentStacheCosmetic = null;
        }
        Cosmetic tempStache = cop.GiveMe(CosID).GetComponent<Cosmetic>();
        if (tempStache)
        {
            currentStacheCosmetic = tempStache;
            currentStacheCosmetic.transform.parent = head;
            currentStacheCosmetic.transform.localPosition = Vector3.zero;
            currentStacheCosmetic.transform.localEulerAngles = Vector3.zero;
            Animator stacheAnim = tempStache.GetComponentInChildren<Animator>();
            if (stacheAnim)
            {
                h.anim = stacheAnim;
            }
        }
    }

    public void GiveMeSpatula(string CosID)
    {
        if (currentSpatulaCosmetic)
        {
            cop.Recall(currentSpatulaCosmetic);
            currentSpatulaCosmetic = null;
        }
        Cosmetic tempSpat = cop.GiveMe(CosID).GetComponent<Cosmetic>();
        if (tempSpat)
        {
            currentSpatulaCosmetic = tempSpat;
            currentSpatulaCosmetic.transform.parent = spatula;
            currentSpatulaCosmetic.transform.localPosition = Vector3.zero;
            currentSpatulaCosmetic.transform.localEulerAngles = Vector3.zero;
        }
    }

    public void GiveMeKnife(string CosID)
    {
        if (currentKnifeCosmetic)
        {
            cop.Recall(currentKnifeCosmetic);
            currentKnifeCosmetic = null;
        }
        Cosmetic tempKnife = cop.GiveMe(CosID).GetComponent<Cosmetic>();
        if (tempKnife)
        {
            currentKnifeCosmetic = tempKnife;
            currentKnifeCosmetic.transform.parent = knife;
            currentKnifeCosmetic.transform.localPosition = Vector3.zero;
            currentKnifeCosmetic.transform.localEulerAngles = Vector3.zero;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class AudioClips {
    //a neat array of audio clips

    public string audioName;

    //in hindsight naming the class AudioClips, one character off a default class might not have been optimal
    public AudioClip[] audioClip;

    [Range(0f, 1f)]
    public float audioVolume = 1;

    //[HideInInspector]
    public AudioSource audioSource;

    public bool isLooping;

    public bool randomLoop;

    public bool playOnAwake;

    public GameObject reference;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PostGameStatBoard : MonoBehaviour {


    public Animator anim;

    public Text playerNameTxt;
    public Text gameStatsTxt;
    public Text cheeseStatsTxt;

    // Use this for initialization
    public void OpenPostGameStats()
    {
        anim.SetBool("Visible", true);
    }

    public void ClosePostGameStats()
    {
        anim.SetBool("Visible", false);
    }

    public void UpdateUI(GameSession gs, StatSheet ss)
    {
        playerNameTxt.text = gs.player;
        gameStatsTxt.text = ("Game: " + ss.gamesPlayed + "\n" + "Score: " + gs.score);
        cheeseStatsTxt.text = ("Eaten: " + gs.totalCheeseEaten + "\n" + "Squashed: " + gs.totalCheeseSquashed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextCopier : MonoBehaviour {

    public Text textToCopy;
    public InputField inputF;
    public Text textToDisplay;
	// Use this for initialization
	// Update is called once per frame
	void LateUpdate () {

        if (inputF)
        {
            if (inputF.isFocused)
            {
                textToCopy.text = inputF.text;
            }
            else
            {
                inputF.text = textToCopy.text;
            }
        }
        else
        {
            if (textToDisplay.text != textToCopy.text)
            {
                textToDisplay.text = textToCopy.text;
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBoardMover : MonoBehaviour {

    public float moveSpeed = 2;
    public Transform playerhead;
    public float maxOffsetFromPlayer = 5.5f;
    public float minOffsetFromplayer = 0.25f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.z > playerhead.position.z + maxOffsetFromPlayer)
        {
            transform.position -= new Vector3(0, 0, moveSpeed * Time.deltaTime);
        }
        else if (transform.position.z < playerhead.position.z + minOffsetFromplayer)
        {
            transform.position += new Vector3(0, 0, moveSpeed * Time.deltaTime);
        }
	}

    private void OnDrawGizmosSelected()
    {
        if (playerhead)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(new Vector3(0, 0, playerhead.position.z + minOffsetFromplayer), 0.1f);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(new Vector3(0, 0, playerhead.position.z + maxOffsetFromPlayer), 0.1f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMe : MonoBehaviour {

    public GameObject m_ToFollow;


	// Update is called once per frame
	void Update () {
        this.transform.position = m_ToFollow.transform.position;

	}
}

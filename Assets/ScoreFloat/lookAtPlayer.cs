﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtPlayer : MonoBehaviour {

    Transform player;

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {
            player = Camera.main.transform;
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(transform.position - player.position);
        }
    }
}

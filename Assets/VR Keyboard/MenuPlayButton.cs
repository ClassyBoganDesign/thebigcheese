﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayButton : VRButton {

    GameManager gm;
	// Use this for initialization
	void Awake () {
        gm = FindObjectOfType<GameManager>();
        OnKeyPressed.AddListener(StartGame);
	}

    public void StartGame()
    {
        gm.CloseMainMenu();
        gm.OpenKeys();
        gm.CloseStatBoard();
    }
}

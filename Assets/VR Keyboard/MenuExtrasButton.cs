﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuExtrasButton : VRButton {

    GameManager gm;
    public Animator extrasMiniMenuAnim;
    bool miniMenuActive = false;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        OnKeyPressed.AddListener(ToggleMiniMenu);
    }

    void ToggleMiniMenu()
    {
        miniMenuActive = !miniMenuActive;
        extrasMiniMenuAnim.SetBool("Visible", miniMenuActive);
    }

    private void OnDisable()
    {
        miniMenuActive = false;
        extrasMiniMenuAnim.SetBool("Visible", miniMenuActive);
    }
}

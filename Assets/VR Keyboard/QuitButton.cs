﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : VRButton {

    private void Awake()
    {
        OnKeyPressed.AddListener(quitApp);
    }

    void quitApp()
    {
        Application.Quit();
    }
}

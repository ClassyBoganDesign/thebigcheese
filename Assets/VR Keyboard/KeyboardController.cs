﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class Confirm : UnityEvent<string>
{

}

public class KeyboardController : MonoBehaviour {

    public Text currentField;
    public Confirm onConfirm;
    bool Capitals;
    public int letterCap;
    bool letterLocked;
    public float timeBetweenLetters = 0.05f;

    public void Start()
    {
        currentField.text = "Enter Name";
        GameManager gm = FindObjectOfType<GameManager>();
        gm.onActivateKeyboard.AddListener(ResetNameText);
    }

    void ResetNameText()
    {
        currentField.text = "Enter Name";
    }

    public void AddLetter(string letter)
    {
        if (letterLocked)
        {
            return;
        }
        if (currentField.text.ToLower() == "enter name")
        {
            currentField.text = "";
        }
            currentField.text += (Capitals) ? letter.ToUpper() : letter.ToLower();
        
        if (currentField.text.Length >= letterCap)
        {
            BackSpace();
        }
        letterLocked = true;
        Invoke("UnlockLetters", timeBetweenLetters);
    }

    void UnlockLetters()
    {
       letterLocked = false;
    }
    public void BackSpace()
    {
        if (letterLocked)
        {
            return;
        }
        if (currentField.text.Length > 0)
        {
            currentField.text = currentField.text.Substring(0, currentField.text.Length - 1);
        }
        letterLocked = true;
        Invoke("UnlockLetters", timeBetweenLetters);
    }
    public void Confirm()
    {
        if (letterLocked)
        {
            return;
        }
        if (currentField.text != "" && currentField.text.ToLower() != "enter name" && currentField.text.ToLower() != " " && currentField.text.ToLower() != "  " && currentField.text.ToLower() != "   ")
        {
            onConfirm.Invoke(currentField.text);
        }
        letterLocked = true;
        Invoke("UnlockLetters", timeBetweenLetters);
    }
    public void ChangeCaps()
    {
        Capitals = (Capitals) ? false : true;
    }
    public void Clear()
    {
        if (letterLocked)
        {
            return;
        }
        currentField.text = "Enter Name";
        letterLocked = true;
        Invoke("UnlockLetters", timeBetweenLetters);
    }

    public void Drop()
    {

    }
}

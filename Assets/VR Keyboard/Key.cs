﻿

public class Key : VRButton {

    public string keyToAdd;

    KeyboardController kc;

    private void Awake()
    {
        OnKeyPressed.AddListener(Click);
    }

    public void Click()
    {
        if (!kc)
        {
            kc = FindObjectOfType<KeyboardController>();
        }
        if (kc)
        {
            if (keyToAdd.ToLower() == "backspace")
            {
                kc.BackSpace();
            }
            else if (keyToAdd.ToLower() == "confirm")
            {
                kc.Confirm();
            }
            else if (keyToAdd.ToLower() == "clear")
            {
                kc.Clear();
            }
            else if (keyToAdd.ToLower() == "caps")
            {
                kc.ChangeCaps();
            }
            else
            {
                kc.AddLetter(keyToAdd);
            }

        }
    }

}

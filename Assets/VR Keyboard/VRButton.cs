﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VRButton : Cheese {

    public UnityEvent OnKeyPressed;
    public Animator anim;
    public bool pressed;
    public Transform playerHead;

    public Transform originalP;
    public Vector3 localP;
    public Vector3 localR;
    private void Start()
    {
        playerHead = FindObjectOfType<PlayerCosmeticController>().head;
        anim = GetComponent<Animator>();
        anim.speed = Random.Range(0.9f, 1.1f);
        onCheeseSplat.AddListener(ResetPosition);
        originalP = transform.parent;
        localP = transform.localPosition;
        localR = transform.localEulerAngles;
        head = FindObjectOfType<Head>().transform; 
    }

    private void Update()
    {
        if (stabbed)
        {
            anim.SetBool("Pressed", true);
            if (stabbed && Vector3.Distance(transform.position, head.position) < 2f)
            {
                OnKeyPressed.Invoke();
                Splat();
            }
        }
    }

    void ResetPosition()
    {
        Debug.Log("position Reset");
        anim.SetBool("Pressed", false);
        transform.parent = originalP;
        transform.localPosition = localP;
        transform.localEulerAngles = localR;
    }
}

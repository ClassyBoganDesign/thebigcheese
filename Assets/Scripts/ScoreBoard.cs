﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
//using UnityEditor;

public class ScoreBoard : MonoBehaviour {

    public List<Text> scoreText;
    public int[] scoreValues;
    public string[] scoreNames;
    void Start() {
        int arrayLength = scoreText.Count;
        scoreValues = new int[arrayLength];
        scoreNames = new string[arrayLength];
        SubmitScore(100);
    }

    // Save/Load MUST be in order of name->score
    void SaveData() {
        string path = "Assets/scores.txt";
        StreamWriter writer = new StreamWriter(path);

        writer.AutoFlush = true;
        // writes the names and scores into the file
        for (int i = 0; i < scoreText.Capacity;i++) {
            writer.WriteLine(scoreNames[i]);
            Debug.Log("Name Wrote");
            writer.WriteLine(scoreValues[i]);
            Debug.Log("Score Writed");
        }
        writer.Close();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.D)) {
            LoadData();
        }
        if (Input.GetKeyDown(KeyCode.S)) {
            SaveData();
        }
        if (Input.GetKeyDown(KeyCode.F)) {
            ClearData();
        }
    }

    void LoadData() {
        string path = "Assets/scores.txt";

        StreamReader reader = new StreamReader(path);

        // loads each data entry into the list
        for (int i = 0; i < scoreText.Capacity;i++) {
            scoreNames[i] = reader.ReadLine();
            scoreValues[i] = int.Parse(reader.ReadLine());
            
            if (scoreNames[i] == null || scoreNames[i] == "") {
                scoreNames[i] = "Bob";
            }
        }
            Debug.Log("parsed: " + scoreValues[2]);

        for (int i = 0; i < scoreText.Capacity; i++) {
            scoreText[i].text = scoreNames[i] + ":  " + scoreValues[i];
            Debug.Log("Worked");
        }
        // makes sure all data is sorted correctly
        SortScores();
    }

    void ClearData() {
        string path = "Assets/scores.txt";
    
        StreamWriter writer = new StreamWriter(path, false);
        writer.Write("");
        writer.Close();
    }

    public void SubmitScore(int score) {
        // checks if the score goes on the scoreboard
        //if (score > scoreValues[scoreValues.Length - 1]) {
        Debug.Log("SUbmit");
            LoadData();
            int placedIndex = 0;
            for (int i = scoreValues.Length-1; i > -1; i--) {
            Debug.Log("for loop");
            Debug.Log("Index score: " + scoreValues[2]);
                if  (score > scoreValues[i]) {
                    placedIndex = i;
                }
            }
        Debug.Log("score is: " + score);
            scoreValues[placedIndex] = score;
        SortScores();
        //}
    }

    void SortScores() {
        //int temp = -1;
        //while (temp == -1)    {
        //    for (int i = 0; i < scoreValues.Length - 1; i++) {
        //        int a = scoreValues[i];
        //        int b = scoreValues[i + 1];
        //        if (a < b) {
        //            temp = a;
        //            a = b;
        //            b = temp;
        //        }
        //    }
        //}
        for (int i = 0; i < scoreText.Capacity; i++) {
            scoreText[i].text = scoreNames[i] + ":  " + scoreValues[i];

            Debug.Log(scoreValues[i]);
        }
    }
}

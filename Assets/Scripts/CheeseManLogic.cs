﻿// test
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheeseManLogic : Cheese {

    public CheeseData[] cheeseTypes;
    public CheeseData cheeseType;
    public bool mutated;
    public int mutationAttempts = 12;
    public Transform cheeseManHead;

    public float groundedDis = 1.0f;
	bool running;
    public float getUpTime;
    public bool inDanger;
    float getUpTimer;

    public LayerMask splatLayer;

    public Animator anim;
    public UnityEvent onEat;
    Animator moustacheAnim;
    public float distanceBeforeAte = 1f;

	public NavPoint[] navpoints;
	public Transform currentNavPoint;
    public Transform cheeseManRotator;
	Rigidbody rb;
    public Transform threat;
    public float threatTimer;

    public Transform viewPortPos;

    CosmeticObjPool cop;
    List<Cosmetic> cosmetics = new List<Cosmetic>();

    // Use this for initialization
    void Start() {
        st = FindObjectOfType<statsTracker>();
        cop = FindObjectOfType<CosmeticObjPool>();
        gm = FindObjectOfType<GameManager>();
        Head h= FindObjectOfType<Head>();
        moustacheAnim = h.anim;
        head = h.transform;
        
        navpoints = FindObjectsOfType<NavPoint>();
        rb = GetComponent<Rigidbody>();

        ChooseNewNavPoint();
        onCheeseSplat.AddListener(SplatCheeseMan);
	}
    private void OnEnable()
    {
        transform.localScale = Vector3.one;
    }
    float movetimer;
    void Mutate()
    {
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        if (Vector3.Distance(head.position, transform.position) < distanceBeforeAte)
        {
            if (!eaten)
            {
                onEat.Invoke();
                EatThisBoi();
            }
        }
        if (eaten || stabbed)
        {
            if (eaten)
            {
                transform.localPosition = new Vector3(0, -0.75f, 0.5f);
                transform.localEulerAngles = Vector3.zero;
                cheeseManRotator.localEulerAngles = new Vector3(270, 0, 0);
                if (transform.localScale.x > 0.01f)
                {
                    transform.localScale -= Vector3.one * Time.deltaTime;
                }
            }
            return;
        }

        if (Vector3.Distance(transform.position, currentNavPoint.position) > 20f)
        {
            DeInstance();
        }

        if (stabbed)
        {
            if (knife)
                rb.MovePosition(stabSpot.position);
            else
            {
                stabbed = false;
            }
        }
        else
        {
            // if the player tool is nearby, places a nav node away from them
            if (threatTimer >=0 && threat != null)
            {
                anim.SetBool("Dodge", true);
                threatTimer -= Time.deltaTime;
                rb.MovePosition(transform.position + (transform.position - threat.position).normalized * (5f * Time.deltaTime));
            } 
            else
            {
                anim.SetBool("Dodge", false);
                if (inDanger)
                {
                    inDanger = false;
                    ChooseNewNavPoint();
                    return;
                }
                if (Vector3.Distance(transform.position, currentNavPoint.position) >2f)
                {
                    if (!running)
                    {
                        anim.SetBool("Running", true);
                        running = true;
                    }
                    rb.MovePosition(transform.position + (currentNavPoint.position - transform.position).normalized * (2f * Time.deltaTime));
                    cheeseManRotator.rotation = Quaternion.Slerp(cheeseManRotator.rotation, Quaternion.LookRotation(currentNavPoint.position - transform.position), 3 * Time.deltaTime);
                    //transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                    cheeseManRotator.eulerAngles = new Vector3(0, cheeseManRotator.eulerAngles.y, 0);
                }
                else
                {
                    if (running)
                    {
                        anim.SetBool("Running", false);
                        running = false;
                        movetimer = Random.Range(0, 1.5f);
                    }
                    if (movetimer <= 0)
                    {
                        ChooseNewNavPoint();
                    }
                    else
                    {
                        movetimer -= Time.deltaTime;
                    }

                }
            }
            
        }

	}

    GameManager gm;
    void EatThisBoi()
    {
        if (moustacheAnim)
            moustacheAnim.SetTrigger("Eat");
        
        gm.StartGame();
        st.AddEatenCheese(cheeseType.ct);
        transform.parent = head.transform;
        transform.localPosition = new Vector3(0, -0.75f, 0.5f);
        transform.localEulerAngles = Vector3.zero;
        cheeseManRotator.localEulerAngles = new Vector3(270, 0, 0);
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        eaten = true;

        //ParticleSystem eatEffect = cop.GiveMe(cheeseType.eatEffectID).GetComponent<ParticleSystem>();
        //eatEffect.Stop();
        //eatEffect.transform.parent = transform;
        //eatEffect.transform.localPosition = Vector3.zero;
        //eatEffect.transform.localEulerAngles = Vector3.zero;
        //eatEffect.transform.parent = head.transform;
        //eatEffect.Play();
        //Invoke("DeInstance", eatEffect.main.duration + eatEffect.main.startLifetime.constantMax);
    }
    statsTracker st;
    Transform knife = null;
    Transform stabSpot;

    void DeInstance()
    {
        foreach (Cosmetic c in cosmetics)
        {
            cop.Recall(c);
        }
        cosmetics.Clear();
        eaten = false;
        stabbed = false;
        mutated = false;
        anim.SetTrigger("Reset");
        anim.SetBool("Running", false);
        anim.SetBool("Dodge", false);
        transform.parent = null;
        rb.isKinematic = false;
        inDanger = false;
        running = false;
        gameObject.SetActive(false);
    }

    void ChooseNewNavPoint()
    {
        currentNavPoint = navpoints[Random.Range(0, navpoints.Length)].transform;
    }

    public void SplatCheeseMan()
    {
        if (eaten)
        {
            return;
        }
        Debug.Log(gameObject.name + " called splat()");
        RaycastHit hit;
        if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), Vector3.down, out hit, groundedDis, splatLayer))
        {

            //GameObject splat = cop.GiveMe(cheeseType.groundSplatID);
            //splat.transform.position = hit.point;
            //splat.transform.rotation = Quaternion.identity;
        }
        else
        {
            //GameObject splat = cop.GiveMe(cheeseType.airSplatID);
            //splat.transform.position = transform.position;
            //splat.transform.rotation = Quaternion.identity;
        }
        st.AddSquashedCheese(cheeseType.ct);
        eaten = true;
        DeInstance();
    }

    public void SetThreat(Transform t)
    {
        threat = t;
        threatTimer = Random.Range(0.1f, 0.5f);
        inDanger = true;
    }

    public void SetPos(Transform newPos, Vector3 offset)
    {
        stabSpot = new GameObject("stabSpot").transform;
        stabSpot.position = transform.position;
        transform.parent = newPos;
        transform.localPosition = offset;
        transform.localRotation = new Quaternion(0, 0, 0, 0);
        cheeseManRotator.localRotation = new Quaternion(0, 0, 0, 0);
        stabbed = true;
        if (Random.Range(0, 100) < 50)
        {
            anim.SetTrigger("Stabbed 1");
        }
        else
        {
            anim.SetTrigger("Stabbed 2");
        }
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
    }
}

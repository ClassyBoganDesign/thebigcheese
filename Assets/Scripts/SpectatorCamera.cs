﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorCamera : MonoBehaviour {

    public List<Transform> positions;

    public Transform viewTarget;

    public float swapTime;
    float timeRemaining;

    public float lerpTime = 1.2f;

    public Transform curPos;
    public Transform prevPos;

    [Range (0,1)]
    public float lerpIndex;
	// Use this for initialization
	void Start () {
        timeRemaining = swapTime;
        SwapPosition();
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(viewTarget);
        timeRemaining -= Time.deltaTime;

        if (lerpIndex < 1)
        {
            lerpIndex += Time.deltaTime / lerpTime;

            transform.position = Vector3.Lerp(transform.position, curPos.position, lerpIndex);
        }
        if (timeRemaining <= 0)
        {
            SwapPosition();
            timeRemaining = swapTime;
        }
       
	}

    void SwapPosition()
    {
        lerpIndex = 0;

        int r = Random.Range(0, positions.Capacity);
        while (positions[r] == curPos || positions[r] == prevPos)
        {
            r = Random.Range(0, positions.Capacity);
        }
        prevPos = curPos;
        curPos = positions[r];

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseSpawner : MonoBehaviour {

	public SpawnPoint[] spawnPoints;

	public float timeBetweenSpawns;

	public GameObject cheesePrefab;

	public List<GameObject> activeCheeses = new List<GameObject>();

	public int maxSpawnedObjects;

    public List<GameObject> inactiveCheeses = new List<GameObject>();
	// Use this for initialization
	void Start () {
        spawnPoints = FindObjectsOfType<SpawnPoint>();
		InvokeRepeating ("SpawnCheese", 0, timeBetweenSpawns);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void SpawnCheese ()
	{
		PurgeList ();
		if (activeCheeses.Count <= maxSpawnedObjects) {

            GameObject cheese = null;
            GameObject cheeseFromPool = firstAvaliableInactiveCheese();
            if (cheeseFromPool)
            {
                inactiveCheeses.Remove(cheeseFromPool);
                cheese = cheeseFromPool;
            }
            else
            {
                // Cheese Instantiation
                cheese = Instantiate(cheesePrefab);

            }
            SpawnPoint sp = spawnPoints[Random.Range(0, spawnPoints.Length)];

            sp.spawnCheese();
            
            cheese.transform.position = sp.transform.position;
            cheese.SetActive(true);

			activeCheeses.Add (cheese);
	
		}
	}
    GameObject firstAvaliableInactiveCheese()
    {
        if (inactiveCheeses.Count > 0)
        {
            foreach (GameObject obj in inactiveCheeses)
            {
                if (obj)
                {
                    return obj;
                }
            }
        }
        return null;
    }

	void PurgeList()
	{
        for (int i = activeCheeses.Count - 1; i >= 0; i--)
        {
            if (!activeCheeses[i])
            {
                activeCheeses.RemoveAt(i);
            }
            else if (activeCheeses[i] && !activeCheeses[i].activeInHierarchy)
            {
                inactiveCheeses.Add(activeCheeses[i]);
                activeCheeses.RemoveAt(i);
            }
        }
	}
}

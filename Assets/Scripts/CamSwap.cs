﻿using UnityEngine;

public class CamSwap : MonoBehaviour {

    public Camera cam1;
    public Camera cam2;

    public Vector2 primarySize = new Vector2(0.7f, 1f);
    public Vector2 priamryPosition = new Vector2(0,0);

    public Vector2 secondarySize = new Vector2(0.3f, 0.3f);
    public Vector2 secondaryPosition = new Vector2(0.7f,0.7f);

    public int primaryDepth;
    public int secondaryDepth;

    public bool cam1Primary;

    private void Start() {
        SwapCameras();
    }

    public void SwapCameras() {
        if (cam1Primary) {
            cam1Primary = false;

            cam1.rect = new Rect(secondaryPosition, secondarySize);
            cam1.depth = secondaryDepth;

            cam2.rect = new Rect(priamryPosition, primarySize);
            cam2.depth = primaryDepth;
        } else {
            cam1Primary = true;

            cam1.rect = new Rect(priamryPosition, primarySize);
            cam1.depth = primaryDepth;

            cam2.rect = new Rect(secondaryPosition, secondarySize);
            cam2.depth = secondaryDepth;
        }
    }
}

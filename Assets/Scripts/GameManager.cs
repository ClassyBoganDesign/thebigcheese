﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public enum GameState
{
    PreGame,
    InGame,
    PostGame
}


[System.Serializable]
public class highscoreCandidate
{
    public string cName;
    public float cScore;
    public int cRank;
    public bool ranked;
}

[System.Serializable]
public class highscoreComponent
{
    public Text name, dots, score;
}

public class GameManager : MonoBehaviour {

    public GameState gameState;

    public int maxCharacters = 20;

    public float GameDuration;
    public float GameTimer;
    public Text gameTimerTxt;

    public float score;
    public Text scoreTxt;

    public Text HighscoreTxt;


    public GameObject turnThisOffWhenGameStarts;

    [Header("Audio Settings")]
    public AudioManager audioManager;
    public string startAudio;
    public string endAudio;
    public string menuAudio;
    public string pregameAudio;
    public string mainAudio;
    public string mainMenuAudio = "main menu audio";


    [Header("Not Audio Settings")]
    public float resetTimer;

    public ScoreBoard scoreBoard;

    public float comboTime = 1f;
    float comboTimer;
    int combo = 1;

    [System.NonSerialized] public UnityEvent onActivateKeyboard = new UnityEvent();

    public Transform playerHead;
    public LayerMask scoreMask;
    public float scoreFloatTime = 0.5f;
    float scoreFloatTimer;
    float scoreMadeInTime;
    bool scoreFloatReady = false;
    CosmeticObjPool cop;
    public Color poitiveScoreColour;
    public Color negativeScoreColour;

    public Animator mainMenuAnim;
    statsTracker st;
    PostGameStatBoard pgsb;

    public float floorLevel = 0.11f;
    private void Start()
    {
        pgsb = FindObjectOfType<PostGameStatBoard>();
        st = FindObjectOfType<statsTracker>();
        cop = FindObjectOfType<CosmeticObjPool>();
        audioManager = (audioManager) ? audioManager : GetComponent<AudioManager>();
        playerHead = Camera.main.transform;
        
        HighscoreTxt.text = ("Highscore: " + PlayerPrefs.GetFloat("Highscore"));
        scoreTxt.text = "Score: " + 0;
        loadScores();
        EndGame();
    }
    // Update is called once per frame
    void Update () {
        if (gameState == GameState.InGame)
        {
            GameTimer -= Time.deltaTime;
            if (GameTimer < 0)
            {
                GameTimer = 0;
                EndGame();
            }
            gameTimerTxt.text = Mathf.Round (GameTimer).ToString();
        }
        else if (gameState == GameState.PostGame)
        {

        }
        if (comboTimer >= 0)
        {
            comboTimer -= Time.deltaTime;
        }
        else
        {
            combo = 1;
        }

        if (scoreFloatTimer >= 0)
        {
            scoreFloatTimer -= Time.deltaTime;
        }
        else
        {
            if (!scoreFloatReady)
            {
                scoreFloatReady = true;
                RaycastHit hit;
                if (Physics.Raycast(playerHead.position, playerHead.forward, out hit, 100f, scoreMask))
                {
                    GameObject sf = cop.GiveMe("ScoreFloat");
                    sf.transform.position = new Vector3(hit.point.x, hit.point.y + 0.5f, hit.point.z);
                    Text[] sfTexts = sf.GetComponentsInChildren<Text>();
                    foreach (Text sfText in sfTexts)
                    {
                       
                        sfText.text = (scoreMadeInTime).ToString();
                    }
                    sfTexts[1].color = poitiveScoreColour;
                }
                else
                {
                    GameObject sf = cop.GiveMe("ScoreFloat");
                    sf.transform.position = playerHead.position + (playerHead.forward * 10f);
                    Text[] sfTexts = sf.GetComponentsInChildren<Text>();
                    foreach (Text sfText in sfTexts)
                    {
                        
                        sfText.text = (scoreMadeInTime).ToString();
                    }
                    sfTexts[1].color = poitiveScoreColour;
                }
                scoreMadeInTime = 0;
            }
        }

	}
    public void StartGame()
    {
        if (gameState == GameState.PreGame)
        {
            st.RegisterNewGameSession(currentCandidate.cName);
            resetTimer = 2f;
            GameTimer = GameDuration;
            turnThisOffWhenGameStarts.SetActive(false);
            gameState = GameState.InGame;
            PlayAudio(startAudio);
            StopAudio(pregameAudio);
            PlayAudio(mainAudio);
        }
    }

    void PlayAudio(string audioToPlay)
    {
        if (!audioManager)
        {
            audioManager = FindObjectOfType<AudioManager>();
        }
        audioManager.PlayAudio(audioToPlay);
    }

    void StopAudio(string audioToStop)
    {
        if (!audioManager)
        {
            audioManager = FindObjectOfType<AudioManager>();
        }
        audioManager.StopAudio(audioToStop);
    }

    public void EndGame()
    {
        onActivateKeyboard.Invoke();
        st.EndGameSession(currentCandidate.cScore);
        gameState = GameState.PostGame;
        PlayAudio(endAudio);
        StopAudio(mainAudio);
        PlayAudio(menuAudio);
        if (st.currentStatSheet != null && st.currentSession != null)
        {
            pgsb.OpenPostGameStats();
            pgsb.UpdateUI(st.currentSession, st.currentStatSheet);
        }
        else
        {
            OpenMainMenu();
        }
    }

    public void OpenMainMenu()
    {
        mainMenuAnim.SetBool("Visible", true);
        StopAudio(pregameAudio);
        StopAudio(menuAudio);
        PlayAudio(mainMenuAudio);
    }
    public void CloseMainMenu()
    {
        mainMenuAnim.SetBool("Visible", false);
        StopAudio(mainMenuAudio);
    }

    public void OpenKeys()
    {
        nameRegisterAnim.SetBool("Visible", true);
        PlayAudio(pregameAudio);
    }

    public void CloseKeys()
    {
        nameRegisterAnim.SetBool("Visible", false);
    }

    public void OpenStatBoard()
    {
        pgsb.OpenPostGameStats();
    }
    public void CloseStatBoard()
    {
        pgsb.ClosePostGameStats();
    }

    public void AddScore()
    {
        StartGame();
        if (gameState == GameState.InGame)
        {
            comboTimer = comboTime;
            combo++;
            score += 1.25f * combo;
            scoreMadeInTime += 1.25f * combo;
            if (scoreFloatReady)
            {
                scoreFloatTimer = scoreFloatTime;
                scoreFloatReady = false;
            }

            
            if (score > PlayerPrefs.GetFloat("Highscore"))
            {
                PlayerPrefs.SetFloat("Highscore", Mathf.RoundToInt(score));
                HighscoreTxt.text = ("Highscore: " + PlayerPrefs.GetFloat("Highscore"));
            }
            scoreTxt.text = ("Score: " + Mathf.RoundToInt(score));
            
        }
        currentCandidate.cScore = score;
        UpdateHighScoreTable();
    }

    public void RemoveScore()
    {
        if (gameState == GameState.InGame && score > 0)
        {
            score--;
            combo = 1;
            if (score > PlayerPrefs.GetFloat("Highscore"))
            {
                PlayerPrefs.SetFloat("Highscore", score);
                HighscoreTxt.text = ("Highscore: " + PlayerPrefs.GetFloat("Highscore"));
            }
            scoreTxt.text = ("Score: " + score);
        }
        RaycastHit hit;
        if (Physics.Raycast(playerHead.position, playerHead.forward, out hit, 100f, scoreMask))
        {
            GameObject sf = cop.GiveMe("ScoreFloat");
            sf.transform.position = new Vector3(hit.point.x, hit.point.y + 0.5f, hit.point.z);
            Text[] sfTexts = sf.GetComponentsInChildren<Text>();
            foreach (Text sfText in sfTexts)
            {
                sfText.text = "YUCK!";                
            }
            sfTexts[1].color = negativeScoreColour;
        }
        else
        {
            GameObject sf = cop.GiveMe("ScoreFloat");
            sf.transform.position = playerHead.position + (playerHead.forward * 10f);
            Text[] sfTexts = sf.GetComponentsInChildren<Text>();
            foreach (Text sfText in sfTexts)
            {
                sfText.text = "YUCK!";
            }
            sfTexts[1].color = negativeScoreColour;
        }
        currentCandidate.cScore = score;
        UpdateHighScoreTable();
    }

    public void ResetGame()
    {
        //scoreBoard.SubmitScore((int)score);
        score = 0;
        gameState = GameState.PreGame;
        CloseKeys();
        turnThisOffWhenGameStarts.SetActive(true);
        StopAudio(menuAudio);
        //PlayAudio(pregameAudio);

    }

    [Header("TempHighscoreStuff")]
    public highscoreComponent[] highscoreTable;
    public List<highscoreCandidate> hcs = new List<highscoreCandidate>();
    public highscoreCandidate currentCandidate;

    public Animator nameRegisterAnim;
    //public InputField nameRegister;

    void UpdateHighScoreTable()
    {
        foreach (highscoreCandidate hc in hcs)
        {
            hc.ranked = false;
        }
        int rank = 0;
        foreach (highscoreComponent hsc in highscoreTable)
        {
            rank++;
            highscoreCandidate highestScore = new highscoreCandidate();
            foreach (highscoreCandidate hc in hcs)
            {
                if (!hc.ranked && (hc.cScore > highestScore.cScore))
                {
                    highestScore.cRank = 0;
                    highestScore = hc;
                    highestScore.cRank = rank;
                }
            }
            highestScore.cRank = rank;
            highestScore.ranked = true;

            hsc.name.text = (highestScore.cRank + ". " + highestScore.cName);
            hsc.dots.text = "";
            hsc.score.text = Mathf.RoundToInt(highestScore.cScore).ToString();

            PlayerPrefs.SetFloat("HighscoreFloat" + rank, highestScore.cScore);
            PlayerPrefs.SetString("HighscoreString" + rank, highestScore.cName);
        }
    }

    string lineOfDots(string name, string score)
    {
        string dots = "";
        for (int i = 0; i <= maxCharacters - (name.Length + score.Length); i++)
        {
            dots += ".";
        }
        return dots;
    }

    void loadScores()
    {
        
        List<highscoreCandidate> highscoreCandidates = new List<highscoreCandidate>();
        int rank = 0;
        foreach (highscoreComponent hsc in highscoreTable)
        {
            rank++;
            highscoreCandidate hc = new highscoreCandidate();
            hc.cName = PlayerPrefs.GetString("HighscoreString" + rank);
            hc.cScore = PlayerPrefs.GetFloat("HighscoreFloat" + rank);
            highscoreCandidates.Add(hc);
        }
        hcs = highscoreCandidates;
        UpdateHighScoreTable();
    }

    public void createCandidate(string hcName)
    {
        if (hcName.ToLower() == "clearscores")
        {
            clearHighscores();
            hcName = "";
        }
        currentCandidate = new highscoreCandidate();
        currentCandidate.cName = hcName.ToUpper();
        hcs.Add(currentCandidate);
    }

    public void ChangeCandidateName(string newName)
    {
        currentCandidate.cName = newName;
    }

    public void clearHighscores()
    {
        st.RemoveAllStats();
        int rank = 0;
        foreach (highscoreComponent hsc in highscoreTable)
        {
            rank++;
            highscoreCandidate hc = new highscoreCandidate();
            PlayerPrefs.SetString("HighscoreString" + rank, "");
            PlayerPrefs.SetFloat("HighscoreFloat" + rank, 0);
        }
        CloseKeys();
        EndGame();
        loadScores();
    }

}

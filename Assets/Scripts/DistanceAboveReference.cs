﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceAboveReference : MonoBehaviour {

    public Transform reference;
    public Vector3 offset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = reference.position + offset;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomiseAudio : MonoBehaviour {


	public AudioClip[] screams;

	AudioClip lastScream;
	AudioSource aSource;
	// Use this for initialization
	void Start () 
	{
		aSource = GetComponent<AudioSource> ();
        aSource.clip = screams [Random.Range (0, screams.Length)];
		InvokeRepeating ("ChooseNewClip", 0, Random.Range (2, 3));
        aSource.Play ();
	}
	
	// Update is called once per frame
	void ChooseNewClip()
	{
		AudioClip newScrem = screams [Random.Range (0, screams.Length)];
		if (newScrem != lastScream) {
            aSource.clip = newScrem;
			lastScream = newScrem;
            aSource.Play ();
		} else 
		{
			ChooseNewClip ();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpookZone : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        CheeseManLogic cheese = other.GetComponent<CheeseManLogic>();
        if (cheese != null)
        {
            cheese.SetThreat(transform);
        }
    }
}

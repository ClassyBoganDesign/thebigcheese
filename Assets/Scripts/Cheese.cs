﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Cheese : MonoBehaviour
{
    public bool stabbed, stabable = true, eaten, squished;
    public Transform head;
    public UnityEvent onCheeseSplat;
    public UnityEvent onCheeseStab;

    public void Stab(Transform newPos, Vector3 offset)
    {
        transform.parent = newPos;
        transform.localPosition = offset;
        transform.localRotation = new Quaternion(0, 0, 0, 0);
        stabbed = true;
        onCheeseStab.Invoke();
    }

    public void Splat()
    {        
        onCheeseSplat.Invoke();
        Invoke("Unstab", 0.1f);
    }
    void Unstab()
    {
        stabbed = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum cheeseType { Gouda, Moldy, BabyBell, Goat, Swizzard}

public class statsTracker : MonoBehaviour {

    GameManager gm;
    public StatSheet currentStatSheet;
    public GameSession currentSession;
    List<StatSheet> allStatSheets = new List<StatSheet>();
    // Use this for initialization
    void Start() {
        gm = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update() {

    }
    public void RegisterNewGameSession(string name)
    {
        string player = name;
        currentStatSheet = SearchForStatSheet(player);
        currentSession = new GameSession() { player = player, sessionID = Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() };
    }

    public void EndGameSession(float score)
    {
        if (currentSession != null && currentStatSheet != null)
        {
            currentSession.score = score;
            currentStatSheet.gameSessions.Add(currentSession);
            currentStatSheet.CalculateStats();
        }
    }

    private void OnDisable()
    {
        foreach (StatSheet ss in allStatSheets)
        {
            ss.UploadStats();
        }
    }

    public void AddSquashedCheese(cheeseType ct)
    {
        if (gm.gameState == GameState.InGame)
        {
            currentSession.totalCheeseSquashed += 1;
            if (ct == cheeseType.Gouda)
                currentSession.goudaCheeseSquashed += 1;
            else if (ct == cheeseType.Moldy)
                currentSession.moldyCheeseSquashed += 1;
        }
    }

    public void AddEatenCheese(cheeseType ct)
    {
        if (gm.gameState == GameState.InGame)
        {
            currentSession.totalCheeseEaten += 1;
            if (ct == cheeseType.Gouda)
                currentSession.goudaCheeseEaten += 1;
            else if (ct == cheeseType.Moldy)
                currentSession.moldyCheeseEaten += 1;
        }
    }

    StatSheet SearchForStatSheet(string playerName)
    {
        foreach (StatSheet ss in allStatSheets)
        {
            if (ss.playerName == playerName)
            {
                return ss;
            }
        }
        StatSheet newStatSheet = new StatSheet() { playerName = playerName };
        allStatSheets.Add(newStatSheet);
        return newStatSheet;
    }

    public void RemoveAllStats()
    {
        PlayerPrefs.DeleteAll();
        currentStatSheet = null;
        currentSession = new GameSession();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSession
{
    public string player;

    public string sessionID;
    public float score;

    public int totalCheeseEaten;
    public int goudaCheeseEaten;
    public int moldyCheeseEaten;

    public int totalCheeseSquashed;
    public int goudaCheeseSquashed;
    public int moldyCheeseSquashed;
}
[CreateAssetMenu(fileName = "StatSheet", menuName = "StatSheet/NewStatSheet", order = 1)]
public class StatSheet : ScriptableObject {

    public string playerName;
    public List<GameSession> gameSessions = new List<GameSession>();

    [Header("Score Stats")]
    public int gamesPlayed;
    public float totalScore;
    public float averageScore;
    public float highestScore;

    [Header ("Munch Stats")]
    public int totalCheeseEaten;
    public int goudaCheeseEaten;
    public int moldyCheeseEaten;

    [Header ("Squash Stats")]
    public int totalCheeseSquashed;
    public int goudaCheeseSquashed;
    public int moldyCheeseSquashed;



    public void RecallStats()
    {
        totalScore = PlayerPrefs.GetFloat(playerName + " total score");
        gamesPlayed         = PlayerPrefs.GetInt(playerName + " games played");
        averageScore        = PlayerPrefs.GetFloat(playerName + " average score");
        highestScore        = PlayerPrefs.GetFloat(playerName + " highest score");
        totalCheeseEaten    = PlayerPrefs.GetInt(playerName + " total cheese eaten");
        goudaCheeseEaten    = PlayerPrefs.GetInt(playerName + " gouda cheese eaten");
        moldyCheeseEaten    = PlayerPrefs.GetInt(playerName + " moldy cheese eaten");

        totalCheeseSquashed = PlayerPrefs.GetInt(playerName + " total cheese squashed");
        goudaCheeseSquashed = PlayerPrefs.GetInt(playerName + " gouda cheese squashed");
        moldyCheeseSquashed = PlayerPrefs.GetInt(playerName + " moldy cheese squashed");

    }
    public void CalculateStats()
    {
        RecallStats();
       
        foreach (GameSession gs in gameSessions)
        {
            if (playerName.ToLower() != gs.player.ToLower())
                continue;

            gamesPlayed += 1;
            totalScore += gs.score;
            if (gs.score > highestScore)
                highestScore = gs.score;

            totalCheeseEaten += gs.totalCheeseEaten;
            goudaCheeseEaten += gs.goudaCheeseEaten;
            moldyCheeseEaten += gs.moldyCheeseEaten;

            totalCheeseSquashed += gs.totalCheeseSquashed;
            goudaCheeseSquashed += gs.goudaCheeseSquashed;
            moldyCheeseSquashed += gs.moldyCheeseSquashed;
        }
        averageScore = (totalScore / gamesPlayed);
    }

    public void UploadStats()
    {
        PlayerPrefs.SetFloat(playerName + " total score", totalScore);
        PlayerPrefs.SetInt(playerName + " games played", gamesPlayed);
        PlayerPrefs.SetFloat(playerName + " average score", averageScore);
        PlayerPrefs.SetFloat(playerName + " highest score", highestScore);
        PlayerPrefs.SetInt(playerName + " total cheese eaten", totalCheeseEaten);
        PlayerPrefs.SetInt(playerName + " gouda cheese eaten", goudaCheeseEaten);
        PlayerPrefs.SetInt(playerName + " moldy cheese eaten", moldyCheeseEaten);

        PlayerPrefs.SetInt(playerName + " total cheese squashed", totalCheeseSquashed);
        PlayerPrefs.SetInt(playerName + " gouda cheese squashed", goudaCheeseSquashed);
        PlayerPrefs.SetInt(playerName + " moldy cheese squashed", moldyCheeseSquashed);
        gameSessions.Clear();
    }
}
 
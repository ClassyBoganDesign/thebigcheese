﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPosition : MonoBehaviour {

	public Transform target;

	public float moveSpeed;

	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (target && rb)
        {
            rb.MoveRotation(target.rotation);

            rb.MovePosition(Vector3.Lerp (transform.position, target.position, moveSpeed * Time.deltaTime));

        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatula : MonoBehaviour {

    Rigidbody rb;
    public float splatForce;
    
    Vector3 prevPos;

    public float force;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        prevPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 curPos = transform.position;

        force = Vector3.Distance(curPos,prevPos) / Time.deltaTime;

        prevPos = curPos;
	}

    private void OnTriggerEnter(Collider other)
    {
        Cheese cheese = other.gameObject.GetComponentInParent<Cheese>();
        if (cheese != null)
        {
            if (force > splatForce)
            {
                cheese.Splat();
            }
        }
    }
}

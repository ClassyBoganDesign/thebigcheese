﻿using System.Collections.Generic;
using UnityEngine;

public class KnifeBehaviour : MonoBehaviour {

    public List<Transform> positions;

    public Vector3 offset;

    public Cheese[] cheeses;
	// Use this for initialization
	void Start () {
        cheeses = new Cheese[positions.Capacity];
	}
	
	// Update is called once per frame
	void Update () {
	}

    bool IncBack()
    {
        //Clearing the dead cheeses
        for (int i = cheeses.Length - 1; i >= 0; i--)
        {
            if (!cheeses[i] || cheeses[i].eaten || !cheeses[i].gameObject.activeInHierarchy || !cheeses[i].stabbed || !cheeses[i].stabable)
            {
                Debug.Log("'cheese " + i + "' was removed");
                cheeses[i] = null;
            }
        }

        bool allCheeseSpotsTaken = true;
        for (int i = 0; i < cheeses.Length; i++)
        {
            if (cheeses[i] == null)
            {
                allCheeseSpotsTaken = false;
            }
        }
        if (allCheeseSpotsTaken)
        {
            return false;
        }
        for (int i = 1; i < cheeses.Length; i++)
        {
            if (cheeses[i - 1] == null)
            {
                cheeses[i - 1] = cheeses[i];
                cheeses[i] = null;
            }
        }
        for (int i = 0; i < cheeses.Length; i++)
        {
            if (cheeses[i] != null)
            {
                cheeses[i].transform.parent = positions[i];
                cheeses[i].transform.localPosition = offset;
            }
        }
        return true;
    }

    private void OnTriggerStay(Collider other)
    {
        Cheese cheese = other.GetComponentInParent<Cheese>();
        if (cheese != null && !cheese.stabbed && !cheese.eaten && cheese.stabable)
        {
            if (IncBack())
            {
                Debug.Log("worked");
                CheeseManLogic cml = other.GetComponent<CheeseManLogic>();
                if (cml)
                {
                    cml.SetPos(positions[positions.Capacity - 1], offset);
                }
                else
                {
                    cheese.Stab(positions[positions.Capacity - 1], offset);
                }
                cheeses[cheeses.Length - 1] = cheese;

            }
            else
            {
                Debug.Log("failed");
            }
        }
    }
    int knifeSpotIndex;
    Transform checkFreeKnifeSpot(int ksi)
    {
        knifeSpotIndex = ksi;
        if (!positions[knifeSpotIndex].GetComponentInChildren<CheeseManLogic>())
        {
            return positions[knifeSpotIndex];
        }
        else
        {
            if (knifeSpotIndex - 1 >= 0)
            {
                return checkFreeKnifeSpot(knifeSpotIndex - 1);
            }
            else
            {
                return null;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRecaller : MonoBehaviour {

    public float AutoRecallTime = 5f;
    float recallTimer = 5f;
    CosmeticObjPool cop;
    Cosmetic c;
    private void Start()
    {
        c = GetComponent<Cosmetic>();
        cop = FindObjectOfType<CosmeticObjPool>();
    }
    private void OnEnable()
    {
        recallTimer = AutoRecallTime;
    }
    private void Update()
    {
        if (recallTimer <= 0 && c.claimed)
        {
            recallSelf();
        }
        else
        {
            recallTimer -= Time.deltaTime;
        }
    }
    void recallSelf()
    {
        cop.Recall(c);
    }
}

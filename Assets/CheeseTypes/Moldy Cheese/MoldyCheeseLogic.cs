﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoldyCheeseLogic : Cheese {

    statsTracker st;
    CosmeticObjPool cop;
    GameManager gm;
    Animator moustacheAnim;
    //initializationVariables
    [Header("Initialisation Variables")]
    public CheeseData cheeseData;
    public Transform cosmeticBone;
    private List<Cosmetic> cosmetics = new List<Cosmetic>();
    private Animator anim;

    //CheeseStats
    [Header("local CheeseStats")]
    public float moveSpeed = 2f;
    public float scale = 1f;
    public float turnSpeed;
    [Range(0, 1)]
    public float randomisePercent = 0.1f;
    public bool running;

    //Navigation
    [Header("Navigation Settings")]
    public Vector3 currentMoveTarget;
    public LayerMask floorLayer;
    public float navigationRadius;
    public float actionTime;
    private float actionTimer;
    private float attempts = 10;
    public float minimumDistance = 0.5f;
    //sounds
    [Header("Sounds")]
    public AudioManager am;

    //Particles
    [Header("effects")]
    public GameObject visuals;
    public ParticleSystem eatEffect;
    public ParticleSystem standardSplatEffect;
    public ParticleSystem airSplatEffect;
    public ParticleSystem runEffect;
    public ParticleSystem stabEffect;

    //Death
    [Header("Death")]
    public bool dead;
    public float eatRadius = 1.5f;
    public float timeUntilDespawn = 5;
    float despawnTimer;

    [Header("Grounded Settings")]
    public float floorLevel;
    public bool grounded;
    public Vector3 lastGroundedPosition;
    Rigidbody rb;
    public Vector3 eatPositionOffset;
    public Vector3 stabbedOffset;

    // Use this for initialization
    void Start () {
        rb = GetComponentInChildren<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        st = FindObjectOfType<statsTracker>();
        cop = FindObjectOfType<CosmeticObjPool>();
        gm = FindObjectOfType<GameManager>();
        Head h = FindObjectOfType<Head>();
        moustacheAnim = h.anim;
        head = h.transform;

        if (!am)
            am = GetComponent<AudioManager>();

        onCheeseSplat.AddListener(OnSplat);
        onCheeseStab.AddListener(OnStab);
        InitializeCheese();
	}

    void InitializeCheese()
    {
        //choose cosmetics
        if (cheeseData.cosmetics.Length > 0)
        {
            foreach (Prop p in cheeseData.cosmetics)
            {
                if (Random.Range(0, 100) <= p.chanceToSpawn)
                {
                    GameObject prop = cop.GiveMe(p.cosmeticID);
                    if (prop != null)
                    {
                        prop.transform.parent = cosmeticBone;
                        prop.transform.localPosition = Vector3.zero;
                        prop.transform.localEulerAngles = Vector3.zero;
                        cosmetics.Add(prop.GetComponent<Cosmetic>());
                    }
                }
            }
        }
        //randomise stats slightly

        //randomise Move Speed
        float rMS = (cheeseData.moveSpeed * randomisePercent);
        moveSpeed = Random.Range(cheeseData.moveSpeed - rMS, cheeseData.moveSpeed + rMS);

        //Randomise Scale
        float rS = (cheeseData.baseScale * randomisePercent);
        scale = Random.Range(cheeseData.baseScale - rS, cheeseData.baseScale + rS);

        //RandomiseTurnSpeed
        float rTS = (cheeseData.turnSpeed * randomisePercent);
        turnSpeed = Random.Range(cheeseData.turnSpeed - rTS, cheeseData.turnSpeed + rTS);

    }

    private void OnEnable()
    {
        dead = false;
        ResetCheese();
        InitializeCheese();
        anim.SetTrigger("Spawn");
        visuals.SetActive(true);
        lastGroundedPosition = transform.position;
    }


    private void ResetCheese()
    {
        foreach (Cosmetic c in cosmetics)
        {
            cop.Recall(c);
        }
        cosmetics.Clear();
        eaten = false;
        stabbed = false;
        stabable = true;
        squished = false;
        transform.parent = null;
        running = false;
        anim.SetBool("Stabbed", false);
        visuals.transform.localScale = Vector3.one;
    }

    //when the cheese is splatted...
    void OnSplat()
    {
        if (squished)
        {
            return;
        }
        gm.AddScore();
        squished = true;
        if (stabbed)
        {
            if (airSplatEffect)
                airSplatEffect.Play();
            anim.SetTrigger("Air Splat");
            //visuals.SetActive(false);
            Despawn();
        }
        else
        {
            transform.position = new Vector3 (transform.position.x, floorLevel, transform.position.z);
            if (standardSplatEffect)
                standardSplatEffect.Play();
            anim.SetTrigger("Ground Splat");
            //visuals.SetActive(false);
            Despawn();
        }
        st.AddSquashedCheese(cheeseData.ct);
        
    }
    void OnStab()
    {
        stabbed = true;
        if (stabEffect)
            stabEffect.Play();
        anim.SetBool("Stabbed", stabbed);
        transform.localPosition = stabbedOffset;
    }

    void Despawn()
    {
        despawnTimer = timeUntilDespawn;
        dead = true;
        stabable = false;
        
    }
    void OnEat()
    {
        eaten = true;
        if (eatEffect)
            eatEffect.Play();
        anim.SetTrigger("Eat");
        gm.RemoveScore();
        st.AddEatenCheese(cheeseData.ct);
        transform.parent = head;
        transform.localPosition = eatPositionOffset;
        transform.localEulerAngles = Vector3.zero;
        Despawn();
    }

    void CheckGrounded()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), Vector3.down, out hit, 2, floorLayer))
        {
            floorLevel = hit.point.y;
            if (!grounded)
            {
                grounded = true;
                anim.SetTrigger("Spawn");
                anim.SetBool("Stabbed", false);
            }
            lastGroundedPosition = hit.point;
            rb.isKinematic = true;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y);
        }
        else
        {
            grounded = false;
            anim.SetBool("Stabbed", true);
            rb.isKinematic = false;
            rb.useGravity = true;
        }
    }

    private void Update()
    {
        if (dead)
        {
            despawnTimer -= Time.deltaTime;
            if (despawnTimer < 0)
            {
                transform.parent = null;
                gameObject.SetActive(false);
            }
            return;
        }

        
        if (Vector3.Distance(head.position, transform.position) < minimumDistance)
        {
            OnEat();
        } 
        if (!stabbed && !eaten && !squished)
        {
            if (grounded)
            {
                if (actionTimer <= 0)
                {
                    //choose new target
                    attempts = 10;
                    ChooseNewMoveTarget();

                }
                else
                {
                    running = false;
                    //count down an action timer
                    actionTimer -= Time.deltaTime;
                    //run towards desired target or idle if the target is too close
                    if (currentMoveTarget != Vector3.zero)
                    {

                        float dis = Vector3.Distance(transform.position, currentMoveTarget);
                        if (dis > minimumDistance)
                        {
                            running = true;
                            Quaternion dir = Quaternion.LookRotation((currentMoveTarget - transform.position).normalized);
                            transform.rotation = Quaternion.Slerp(transform.rotation, dir, turnSpeed * Time.deltaTime);
                            transform.position += transform.forward * moveSpeed * Time.deltaTime;
                        }
                        if (dis > 10f)
                        {
                            Despawn();
                        }
                    }
                    else
                    {
                        attempts = 10;
                        ChooseNewMoveTarget();
                    }
                    anim.SetBool("Running", running);

                }

               
            }
            CheckGrounded();
            transform.position = (grounded) ? lastGroundedPosition : transform.position;

        }
    }

    void ChooseNewMoveTarget()
    {
        attempts--;
        float randomX = Random.Range(transform.position.x - navigationRadius, transform.position.x + navigationRadius);
        float randomZ = Random.Range(transform.position.z - navigationRadius, transform.position.z + navigationRadius);

        Vector3 randomSkyPos = new Vector3(randomX, 10, randomZ);

        RaycastHit hit;

        if (Physics.Raycast(randomSkyPos, Vector3.down, out hit, 12, floorLayer))
        {
            currentMoveTarget = new Vector3 (hit.point.x, transform.position.y, hit.point.z);
        }
        else
        {
            if (attempts <= 0)
            {
                currentMoveTarget = transform.position;
            }
            else
            {
                ChooseNewMoveTarget();
            }
        }
        //Reset Timer
        actionTimer = actionTime;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyBellCheeseLogic : Cheese {

    statsTracker st;
    CosmeticObjPool cop;
    GameManager gm;
    Animator moustacheAnim;
    //initializationVariables
    [Header("Initialisation Variables")]
    public CheeseData cheeseData;
    public Transform cosmeticBone;
    private List<Cosmetic> cosmetics = new List<Cosmetic>();
    private Animator anim;

    //CheeseStats
    [Header("local CheeseStats")]
    public float moveSpeed = 2f;
    public float scale = 1f;
    public float turnSpeed;
    [Range(0, 1)]
    public float randomisePercent = 0.1f;
    public bool running;

    //Navigation
    [Header("Navigation Settings")]
    public StandardCheeseLogic currentTarget;
    public LayerMask floorLayer;
    public LayerMask cheeseLayer;
    public float minimumDistance = 0.5f;
    public bool attached = false;
    public List<StandardCheeseLogic> nearbyCheeses = new List<StandardCheeseLogic>();

    //sounds
    [Header("Sounds")]
    public AudioManager am;

    //Particles
    [Header("effects")]
    public GameObject visuals;
    public ParticleSystem eatEffect;
    public ParticleSystem standardSplatEffect;
    public ParticleSystem airSplatEffect;
    public ParticleSystem runEffect;
    public ParticleSystem stabEffect;

    //Death
    [Header("Death")]
    public bool dead;
    public float eatRadius = 1.5f;
    public float timeUntilDespawn = 5;
    float despawnTimer;

    [Header("Grounded Settings")]
    public float floorLevel;
    public bool grounded;
    public Vector3 lastGroundedPosition;
    Rigidbody rb;
    public Vector3 eatPositionOffset;
    public Vector3 AttachedOffset;

    // Use this for initialization
    void Start () {
        rb = GetComponentInChildren<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        st = FindObjectOfType<statsTracker>();
        cop = FindObjectOfType<CosmeticObjPool>();
        gm = FindObjectOfType<GameManager>();
        Head h = FindObjectOfType<Head>();
        moustacheAnim = h.anim;
        head = h.transform;

        if (!am)
            am = GetComponent<AudioManager>();

        onCheeseSplat.AddListener(OnSplat);
        onCheeseStab.AddListener(OnStab);
        InitializeCheese();
	}

    void InitializeCheese()
    {
        //choose cosmetics
        if (cheeseData.cosmetics.Length > 0)
        {
            foreach (Prop p in cheeseData.cosmetics)
            {
                if (Random.Range(0, 100) <= p.chanceToSpawn)
                {
                    GameObject prop = cop.GiveMe(p.cosmeticID);
                    if (prop != null)
                    {
                        prop.transform.parent = cosmeticBone;
                        prop.transform.localPosition = Vector3.zero;
                        prop.transform.localEulerAngles = Vector3.zero;
                        cosmetics.Add(prop.GetComponent<Cosmetic>());
                    }
                }
            }
        }
        //randomise stats slightly

        //randomise Move Speed
        float rMS = (cheeseData.moveSpeed * randomisePercent);
        moveSpeed = Random.Range(cheeseData.moveSpeed - rMS, cheeseData.moveSpeed + rMS);

        //Randomise Scale
        float rS = (cheeseData.baseScale * randomisePercent);
        scale = Random.Range(cheeseData.baseScale - rS, cheeseData.baseScale + rS);

        //RandomiseTurnSpeed
        float rTS = (cheeseData.turnSpeed * randomisePercent);
        turnSpeed = Random.Range(cheeseData.turnSpeed - rTS, cheeseData.turnSpeed + rTS);

    }

    private void OnEnable()
    {
        dead = false;
        ResetCheese();
        InitializeCheese();
        anim.SetTrigger("Spawn");
        visuals.SetActive(true);
        lastGroundedPosition = transform.position;
    }


    private void ResetCheese()
    {
        foreach (Cosmetic c in cosmetics)
        {
            cop.Recall(c);
        }
        cosmetics.Clear();
        eaten = false;
        stabbed = false;
        attached = false;
        squished = false;
        transform.parent = null;
        running = false;
        anim.SetBool("Attached", false);
        visuals.transform.localScale = Vector3.one;
    }

    //when the cheese is splatted...
    void OnSplat()
    {
        //nothing, Can't be squashed
        
    }
    void OnStab()
    {
        //Cant be stabbed
    }

    void Despawn()
    {
        despawnTimer = timeUntilDespawn;
        dead = true;  
    }
    void OnEat()
    {
        eaten = true;
        if (eatEffect)
            eatEffect.Play();
        anim.SetTrigger("Eat");
        gm.AddScore();
        st.AddEatenCheese(cheeseData.ct);
        transform.parent = head;
        transform.localPosition = eatPositionOffset;
        transform.localEulerAngles = Vector3.zero;
        Despawn();
    }

    void CheckGrounded()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), Vector3.down, out hit, 2, floorLayer))
        {
            floorLevel = hit.point.y;
            if (!grounded)
            {
                grounded = true;
                anim.SetTrigger("Spawn");
                anim.SetBool("Stabbed", false);
            }
            lastGroundedPosition = hit.point;
            rb.isKinematic = true;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y);
        }
        else
        {
            grounded = false;
            anim.SetBool("Stabbed", true);
            rb.isKinematic = false;
            rb.useGravity = true;
        }
    }

    private void Update()
    {
        if (dead)
        {
            despawnTimer -= Time.deltaTime;
            if (despawnTimer < 0)
            {
                transform.parent = null;
                gameObject.SetActive(false);
            }
            return;
        }


        if (Vector3.Distance(head.position, transform.position) < minimumDistance)
        {
            OnEat();
        }
        if (attached)
        {
            if (currentTarget.dead)
            {
                transform.parent = null;
                gameObject.SetActive(false);
            }
            if (currentTarget.eaten)
            {
                OnEat();
            }
            transform.localPosition = AttachedOffset;
            transform.localEulerAngles = Vector3.zero;
            return;
        }
        if (!stabbed && !eaten && !squished)
        {
            if (grounded)
            {
                if (!currentTarget)
                {
                    //choose new target
                    ChooseNewMoveTarget();
                    running = false;

                }
                else
                {
                    running = false;
                    //run towards desired target or idle if the target is too close

                        float dis = Vector3.Distance(transform.position, currentTarget.transform.position);
                    if (dis > minimumDistance)
                    {
                        running = true;
                        Quaternion dir = Quaternion.LookRotation((currentTarget.transform.position - transform.position).normalized);
                        transform.rotation = Quaternion.Slerp(transform.rotation, dir, turnSpeed * Time.deltaTime);
                        transform.position += transform.forward * moveSpeed * Time.deltaTime;
                    }
                    else
                    {

                        //attach to raycast hit
                        transform.parent = currentTarget.transform;

                        attached = true;
                        //anim.SetBool("Attached", true);
                        //become child of cheese

                    }
                    if (dis > 10f)
                    {
                            Despawn();
                    }
                    anim.SetBool("Running", running);
                }

               
            }
            CheckGrounded();
            transform.position = (grounded) ? lastGroundedPosition : transform.position;

        }
    }

    void ChooseNewMoveTarget()
    {
        if (nearbyCheeses.Count <= 0)
        {
            currentTarget = null;
        }
        else
        {
            int ri = Random.Range(0, nearbyCheeses.Count);
            if (nearbyCheeses[ri].gameObject.activeInHierarchy && !nearbyCheeses[ri].stabbed && !nearbyCheeses[ri].eaten && !nearbyCheeses[ri].dead && nearbyCheeses[ri].grounded && !nearbyCheeses[ri].squished)
            {
                currentTarget = nearbyCheeses[ri];
            }
            else
            {
                nearbyCheeses.RemoveAt(ri);
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        StandardCheeseLogic scl = other.GetComponentInParent<StandardCheeseLogic>();
        if (scl && scl.gameObject.activeInHierarchy && !scl.stabbed && !scl.eaten && !scl.squished && !scl.dead && scl.grounded)
        {
            foreach (StandardCheeseLogic cl in nearbyCheeses)
            {
                if (cl == scl)
                {
                    return;
                }
            }
            nearbyCheeses.Add(scl);
        }
    }
}

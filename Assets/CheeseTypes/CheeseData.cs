﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Prop
{
    [Range(0, 100)]
    public float chanceToSpawn;
    public string cosmeticID;
}
[CreateAssetMenu(fileName = "CheeseData", menuName = "CheeseType/NewCheeseData", order = 1)]
public class CheeseData : ScriptableObject {

    public cheeseType ct;

    [Header("stats")]
    public float moveSpeed = 2f;
    public float baseScale = 1f;
    public float turnSpeed = 2f;

    [Header("Cosmetics")]
    public Prop[] cosmetics;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CosmeticCollection
{
    public Cosmetic original;
    public List<Cosmetic> copies = new List<Cosmetic>();
}

public class CosmeticObjPool : MonoBehaviour {

    public List <CosmeticCollection> cosmeticCollections = new List<CosmeticCollection>();

    private void Start()
    {
        Cosmetic[] tempCos = GetComponentsInChildren<Cosmetic>();

        foreach (Cosmetic c in tempCos)
        {
            CosmeticCollection TestCC = alreadyInCollection(c.cosmeticID);
            if (TestCC.original != null)
            {
                TestCC.copies.Add(c);
            }
            else
            {
                CosmeticCollection newCC = new CosmeticCollection { original = c };
                cosmeticCollections.Add(newCC);
            }
            c.gameObject.SetActive(false);
        }
    }

    CosmeticCollection alreadyInCollection(string cosID)
    {
        if (cosmeticCollections.Count > 0)
        {
            foreach (CosmeticCollection cc in cosmeticCollections)
            {
                if (cc.original.cosmeticID == cosID)
                {
                    return cc;
                }
            }
        }
        return new CosmeticCollection {original = null };
    }

    Cosmetic firstUnclaimedCosmetic(CosmeticCollection cc)
    {
        foreach (Cosmetic c in cc.copies)
        {
            
            if (c && !c.claimed)
            {
                return c;
            }
        }
        Cosmetic newC = Instantiate(cc.original);
        newC.claimed = false;
        cc.copies.Add(newC);
        return newC;
    }


    public GameObject GiveMe(string cosID)
    {
        CosmeticCollection cc = alreadyInCollection(cosID);
        if (cc.original != null)
        {
            Cosmetic c = firstUnclaimedCosmetic(cc);
            c.claimed = true;
            c.gameObject.SetActive(true);
            return c.gameObject;
        }
        return null;
    }

    public void Recall(Cosmetic cos)
    {
        cos.claimed = false;
        cos.transform.parent = this.transform;
        cos.transform.position = alreadyInCollection(cos.cosmeticID).original.transform.position;
        cos.gameObject.SetActive(false);
    }
}
